﻿namespace DLanguageTim3BackEnd.DTOs
{
    public class UserRegistrationDto
    {
        public string nama { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string verifpassword { get; set; }
    }
}
