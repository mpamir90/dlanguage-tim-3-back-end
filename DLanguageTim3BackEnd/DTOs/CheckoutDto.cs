﻿namespace DLanguageTim3BackEnd.DTOs
{
    public class CheckoutDto
    {
        public Guid IdUser { get; set; }
        public int IdClass { get; set; }
        public DateTime schedule{ get; set; }
    }
}
