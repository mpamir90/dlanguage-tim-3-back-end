﻿namespace DLanguageTim3BackEnd.DTOs
{
    public class NewPasswordDto
    {
        public string password { get; set; }
        public string verifpassword { get; set; }
    }
}
