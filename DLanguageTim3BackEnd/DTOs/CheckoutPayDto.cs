﻿using DLanguageTim3BackEnd.Models;

namespace DLanguageTim3BackEnd.DTOs
{
    public class CheckoutPayDto
    {
        public Guid id_user { get; set; }
        public List<CheckoutItem> list_checkout { get; set; } = new List<CheckoutItem>();

    }

    public class CheckoutItem
    {
        public int id { get; set; }
        public int id_class { get; set; }
        public int price { get; set; }
        public DateTime? schedule { get; set; }
    }


}
