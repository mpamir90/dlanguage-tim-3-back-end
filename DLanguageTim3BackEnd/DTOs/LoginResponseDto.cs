﻿namespace DLanguageTim3BackEnd.DTOs
{
    public class LoginResponseDto
    {
        public string? Token { get; set; }
    }
}
