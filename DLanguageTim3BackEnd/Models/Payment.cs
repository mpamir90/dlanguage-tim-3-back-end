﻿namespace DLanguageTim3BackEnd.Models
{
    public class Payment
    {
        public int id { get; set; }
        public string payment { get; set; } = string.Empty;
        public bool is_active { get; set; }
        public string img_url { get; set; } = string.Empty;
    }
}
