﻿namespace DLanguageTim3BackEnd.Models
{
    public class MyClassModel
    {

        public int Id { get; set; }
        public int IdClass { get; set; }
        public DateTime Schedule { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string Name { get; set; } = string.Empty;
        public string ImgUrl { get; set; } = string.Empty;
        public int Price { get; set; }
        public string Category { get; set; } = string.Empty;

    }
}
