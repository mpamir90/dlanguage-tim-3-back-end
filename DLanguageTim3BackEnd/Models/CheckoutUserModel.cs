﻿namespace DLanguageTim3BackEnd.Models
{
    public class CheckoutUserModel
    {
        public int id { get; set; }
        public int id_class { get; set; }
        public string name { get; set; } = string.Empty;
        public int price{ get; set; }
        public string category { get; set; } = string.Empty;
        public string img_url { get; set; } = string.Empty;
        public DateTime? schedule { get; set; }
    }
}
