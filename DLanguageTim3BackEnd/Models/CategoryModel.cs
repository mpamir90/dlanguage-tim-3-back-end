﻿namespace DLanguageTim3BackEnd.Models
{
    public class CategoryModel
    {
        public int Id { get; set; }
        public string Category { get; set; } = string.Empty;
        public string ImgUrl { get; set; } = string.Empty;
        public string BigImgUrl { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
    }
}
