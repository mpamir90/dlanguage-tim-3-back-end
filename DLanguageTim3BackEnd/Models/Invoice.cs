﻿namespace DLanguageTim3BackEnd.Models
{
    public class Invoice
    {
        public int id { get; set; }
        public string id_user { get; set; } = string.Empty;
        public string no_invoice { get; set; } = string.Empty;
        public int total_price { get; set; }
        public int qty { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
    }
}
