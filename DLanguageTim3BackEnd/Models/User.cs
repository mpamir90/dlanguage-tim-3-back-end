﻿namespace DLanguageTim3BackEnd.Models
{
    public class User
    {
        public Guid id { get; set; }
        public string nama { get; set; } = string.Empty;
        public string email { get; set; } = string.Empty;
        public string password { get; set; } = string.Empty;
        public bool is_active { get; set; }
        public string role { get; set; } = string.Empty;
    }
}
