﻿using Org.BouncyCastle.Bcpg.OpenPgp;

namespace DLanguageTim3BackEnd.Models
{

    public class InvoiceDetailModel
    {
        public string no_invoice { get; set; } = string.Empty;
        public DateTime created_at { get; set; }
        public int total_price { get; set; }
        public List<InvoiceDetailClassItem> list_class { get; set; } = new List<InvoiceDetailClassItem>();
    }
    public class InvoiceDetailClassItem
    {
        public string name { get; set; } = string.Empty;
        public string category { get; set; } = string.Empty;
        public DateTime schedule { get; set; }
       public int price { get; set; }
    }
}

