﻿using Org.BouncyCastle.Bcpg.OpenPgp;

namespace DLanguageTim3BackEnd.Models
{
    public class ClassModel
    {
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;

        public int IdCategory { get; set; }

        public int price { get; set; }
        public string ImgUrl { get; set; } = string.Empty;
        public string category { get; set; } = string.Empty;
        public List<DateTime> ListSchedule { get; set; } = new List<DateTime>();
    }
}
