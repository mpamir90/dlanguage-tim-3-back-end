﻿namespace DLanguageTim3BackEnd.Models
{
    public class CheckoutModel
    {
        public int Id { get; set; }
        public Guid IdUser { get; set; }
        public int IdClass{ get; set; }
        public DateTime schedule{ get; set; }
    }
}
