﻿using DLanguageTim3BackEnd.Models;
using DLanguageTim3BackEnd.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data;

namespace DLanguageTim3BackEnd.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClassController : ControllerBase
    {
        private readonly ClassServices _classServices;
        public ClassController(ClassServices classServices)
        {
            _classServices = classServices;
        }

        [HttpGet("GetAll")]
        public IActionResult GetAll()
        {
            try
            {
                var listClass = _classServices.GetAll();

                return Ok(listClass);
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        [HttpGet("GetAllByCategory")]
        public IActionResult GetAllByCategory([FromQuery] int id_category)
        {
            try
            {
                var listClass = _classServices.GetAllByCategory(id_category);

                return Ok(listClass);
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        [HttpGet("GetById")]
        public IActionResult GetById([FromQuery] int id)
        {
            try
            {
                ClassModel? theClass = _classServices.GetById(id);

                return Ok(theClass);
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public IActionResult AddClass([FromBody] ClassModel classModel)
        {
            _classServices.AddClass(classModel);
            return CreatedAtAction(nameof(GetById), new { id = classModel.Id }, classModel);
        }

        [HttpPut("{id}")]
        [Authorize(Roles = "admin")]
        public IActionResult UpdatePayment(int id, [FromBody] ClassModel classModel)
        {
            try
            {
                bool result = _classServices.UpdateClass(id, classModel);

                if (result)
                {
                    return Ok("Data updated successfully");
                }

                return StatusCode(StatusCodes.Status500InternalServerError, "Failed to update data");
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "admin")]
        public IActionResult Delete(int id)
        {
            _classServices.Delete(id);
            return Ok("Data deteled successfully");
        }

        [HttpGet("GetClassSchedule")]
        public IActionResult GetClassSchedule([FromQuery] string id)
        {
            try
            {
                var theClass = _classServices.GetClassSchedule(id);

                return Ok(theClass);
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }
    }
}
