﻿using DLanguageTim3BackEnd.Models;
using DLanguageTim3BackEnd.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Data;

namespace DLanguageTim3BackEnd.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentController : ControllerBase
    {
        private readonly PaymentServices _paymentSevices;
        public PaymentController(PaymentServices paymentServices)
        {
            _paymentSevices = paymentServices;
        }

        [HttpGet("GetAll")]
        [Authorize]
        public IActionResult GetAll()
        {
            try
            {
                var listPayment = _paymentSevices.GetAll();

                return Ok(listPayment);
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }
        [HttpGet("GetById")]
        [Authorize]
        public IActionResult GetById([FromQuery] int id)
        {
            try
            {
                var Payment = _paymentSevices.GetById(id);

                return Ok(Payment);
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }
        [HttpPost]
        [Authorize(Roles = "admin")]
        public IActionResult AddPayment([FromBody] Payment payment)
        {
            try
            {
                bool result = _paymentSevices.AddPayment(payment);

                if (result)
                {
                    return Ok("Data added successfully");
                }

                return StatusCode(StatusCodes.Status500InternalServerError, "Failed to add data");
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        [HttpPut("{id}")]
        [Authorize(Roles = "admin")]
        public IActionResult UpdatePayment(int id, [FromBody] Payment payment)
        {
            try
            {
                bool result = _paymentSevices.UpdatePayment(id, payment);

                if (result)
                {
                    return Ok("Data updated successfully");
                }

                return StatusCode(StatusCodes.Status500InternalServerError, "Failed to update data");
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "admin")]
        public IActionResult Delete(int id)
        {
            _paymentSevices.Delete(id);
            return Ok("Data deteled successfully");
        }
    }
}
