﻿using DLanguageTim3BackEnd.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DLanguageTim3BackEnd.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MyClassController : ControllerBase
    {
        private readonly MyClassService _myClassService;

        public MyClassController(MyClassService myClassService)
        {
            _myClassService = myClassService;
        }

        [HttpGet]
        public IActionResult GetAllMyClass([FromQuery] Guid id_user)
        {
            try
            {
                var listMyClass = _myClassService.GetAllMyClass(id_user);
                return Ok(listMyClass);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return Problem(ex.Message);
                throw;
            }
            

        }

    }
}
