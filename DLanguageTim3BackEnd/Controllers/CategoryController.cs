﻿using System.Data;
using System.Data.SqlClient;
using DLanguageTim3BackEnd.Models;
using DLanguageTim3BackEnd.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DLanguageTim3BackEnd.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly CategoryService _categorySevices;
        public CategoryController(CategoryService classServices)
        {
            _categorySevices = classServices;
        }

        [HttpGet("GetAll")]
        public IActionResult GetAll()
        {
            try
            {
                var listCategory = _categorySevices.GetAll();

                return Ok(listCategory);
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }
        [HttpGet("GetById")]
        public IActionResult GetById([FromQuery] int id)
        {
            try
            {
                var listCategory = _categorySevices.GetById(id);

                return Ok(listCategory);
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public IActionResult Add([FromBody] CategoryModel category)
        {
            _categorySevices.Add(category);
            return Ok("Data added successfully");
        }

        [HttpPut("{id}")]
        [Authorize(Roles = "admin")]
        public IActionResult Update(int id, [FromBody] CategoryModel category)
        {
            _categorySevices.Update(id, category);
            return Ok("Data updated successfully");
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "admin")]
        public IActionResult Delete(int id)
        {
            _categorySevices.Delete(id);
            return Ok("Data deteled successfully");
        }

    }
}
