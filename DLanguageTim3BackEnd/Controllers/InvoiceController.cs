﻿using DLanguageTim3BackEnd.Models;
using DLanguageTim3BackEnd.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Data;

namespace DLanguageTim3BackEnd.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InvoiceController : ControllerBase
    {
        private readonly InvoiceServices _invoiceServices;

        public InvoiceController(InvoiceServices invoiceServices)
        {
            _invoiceServices = invoiceServices;
        }

        // GET: api/Invoice
        [HttpGet]
        [Authorize(Roles = "admin")]
        public ActionResult<IEnumerable<Invoice>> GetAllInvoices()
        {
            try
            {
                List<Invoice> invoices = _invoiceServices.GetAllInvoices();
                return Ok(invoices);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        // GET: api/Invoice/5
        [HttpGet("{id_user}")]
        [Authorize]
        public ActionResult<IEnumerable<Invoice>> GetInvoiceByIdUser(string id_user)
        {
            try
            {
                List<Invoice> invoices = _invoiceServices.GetInvoiceByIdUser(id_user);
                return Ok(invoices);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        // POST: api/Invoice
        [HttpPost]
        [Authorize]
        public ActionResult<int> CreateInvoice([FromBody] Invoice invoice)
        {
            try
            {
                int invoiceId = _invoiceServices.CreateInvoice(invoice);
                return Ok(invoiceId);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpGet("Detail")]
        [Authorize]
        public ActionResult<InvoiceDetailModel> GetDetailInvoice([FromQuery] string no_invoice)
        {
            try
            {
                InvoiceDetailModel? invoiceDetail = _invoiceServices.GetDetailInvoice(no_invoice);
                return Ok(invoiceDetail);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }
    }
}
