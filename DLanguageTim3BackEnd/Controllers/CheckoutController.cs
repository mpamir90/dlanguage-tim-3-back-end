﻿using DLanguageTim3BackEnd.DTOs;
using DLanguageTim3BackEnd.Models;
using DLanguageTim3BackEnd.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DLanguageTim3BackEnd.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class CheckoutController : ControllerBase
    {       
        private readonly CheckoutService _checkoutService;
        public CheckoutController(CheckoutService checkoutService)
        {
            _checkoutService = checkoutService;
        }

        [HttpPost()]
        public IActionResult AddCheckout([FromBody] CheckoutDto checkoutDto)
        {
            try
            {
                var checkItemFromCheckout = _checkoutService.CheckItemCheckoutUser(checkoutDto.IdUser, checkoutDto.IdClass,checkoutDto.schedule);

                if (checkItemFromCheckout)
                {
                    return Conflict("Item with the same Schedule already exists in the Checkout");
                }

                var checkItemFromMyClass = _checkoutService.CheckItemFromMyClass(checkoutDto.IdUser,checkoutDto.schedule,checkoutDto.IdClass);
                if (checkItemFromMyClass)
                {
                    return Conflict("Item with the same Schedule already exists in the Class");
                }

                var checkoutModel = new CheckoutModel
                {
                    IdClass = checkoutDto.IdClass,
                    schedule = checkoutDto.schedule,
                    IdUser = checkoutDto.IdUser,
                };

                var isSuccess = _checkoutService.AddCheckoutItem(checkoutModel);

                return Ok(isSuccess);   
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }
        [HttpGet("GetAllById")]
        public IActionResult GetAllById([FromQuery] Guid idUser)
        {
            try
            {
                var listCheckout = _checkoutService.GetAllById(idUser);

                return Ok(listCheckout);
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

       

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                _checkoutService.Delete(id);
                return Ok("Data deteled successfully");
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
                throw;
            }
        }

        [HttpPost("PayNow")]
        public IActionResult PayNow([FromBody] CheckoutPayDto pay)
        {
            try
            {
                var res = _checkoutService.PayNow(pay);
                return Ok(res);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
