﻿using BookApi.Emails;
using DLanguageTim3BackEnd.DTOs;
using DLanguageTim3BackEnd.Models;
using DLanguageTim3BackEnd.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Web;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace DLanguageTim3BackEnd.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly UserServices _userService;
        private readonly EmailService _emailService;
        private readonly IConfiguration _configuration;

        public UsersController(UserServices userService, IConfiguration configuration, EmailService emailService)
        {
            _userService = userService;
            _configuration = configuration;
            _emailService = emailService;
        }

        [HttpPost("login")]
        public IActionResult Post([FromBody] UserLoginDto requestDto)
        {
            if (requestDto == null)
                return BadRequest(new { message = "Invalid credential request" });

            if (string.IsNullOrEmpty(requestDto.email) || string.IsNullOrEmpty(requestDto.password))
                return BadRequest(new { message = "Invalid credential request" });

            User user = _userService.CheckUser(requestDto.email);

            if (user == null)
                return Unauthorized(new { message = "You don't have authorization" });

            bool passwordVerification = BCrypt.Net.BCrypt.Verify(requestDto.password, user.password);

            if (!passwordVerification)
                return BadRequest(new { message = "Invalid credential request" });

            if (!user.is_active)
                return Unauthorized(new { message = "You have not activated your account" });

            var secretKey = _configuration.GetSection("JwTConfig:Key").Value;
            var jwtKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secretKey));

            var claims = new Claim[]
            {
                new Claim(ClaimTypes.NameIdentifier, user.id.ToString()),
                new Claim(ClaimTypes.Name, user.nama),
                new Claim(ClaimTypes.Role, user.role),
            };

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.UtcNow.AddDays(20),
                SigningCredentials = new SigningCredentials(jwtKey, SecurityAlgorithms.HmacSha256Signature)
            };

            var tokenHandler = new JwtSecurityTokenHandler();

            var securityToken = tokenHandler.CreateToken(tokenDescriptor);

            string token = tokenHandler.WriteToken(securityToken);

            return Ok(new
            {
                user = new
                {
                    id = user.id,
                    nama = user.nama,
                    role = user.role
                },
                token = token
            });
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody] UserRegistrationDto userInput)
        {
            if (userInput.password != userInput.verifpassword)
                return BadRequest(new { message = "Password does not match" });

            var existingUser = _userService.CheckUser(userInput.email);

            if (existingUser != null)
                return BadRequest(new { message = "User with the same email already exists" });

            var user = new User
            {
                id = Guid.NewGuid(),
                nama = userInput.nama,
                email = userInput.email,
                password = BCrypt.Net.BCrypt.HashPassword(userInput.password),
                is_active = false,
                role = "user"
            };

            bool result = _userService.CreateUserAccount(user);

            if (!result)
                return StatusCode(500, new { message = "Failed to create user account" });

            bool resultSendEmail = await SendEmail(user);
            Console.WriteLine(resultSendEmail);

            return Ok(new { message = "Registration is successful, check email for activation" });
        }

        [HttpPost("create")]
        [Authorize(Roles = "admin")]
        public IActionResult Register([FromBody] User userInput)
        {
            var existingUser = _userService.CheckUser(userInput.email);

            if (existingUser != null)
                return BadRequest(new { message = "User with the same email already exists" });

            var user = new User
            {
                id = Guid.NewGuid(),
                nama = userInput.nama,
                email = userInput.email,
                password = BCrypt.Net.BCrypt.HashPassword(userInput.password),
                is_active = userInput.is_active,
                role = userInput.role
            };

            bool result = _userService.CreateUserAccount(user);

            if (!result)
                return StatusCode(500, new { error = "Failed to create user account" });

            return Ok(new { message = "User account created successfully" });
        }

        [HttpGet]
        [Authorize(Roles = "admin")]
        public IActionResult GetAllUsers()
        {
            List<User> userList = _userService.GetAllUsers();

            return Ok(userList);
        }

        [HttpPut("{id}")]
        [Authorize(Roles = "admin")]
        public IActionResult EditUser(Guid id, [FromBody] User user)
        {
            if (user == null || user.id != id)
                return BadRequest();

            User existingUser = _userService.GetUserById(id);

            if (existingUser == null)
                return NotFound(new { error = "User not found" });

            bool passwordVerification = false;

            if (string.IsNullOrEmpty(user.password))
            {
                // jika input password tidak ada, gunakan password sebelumnya
                user.password = existingUser.password;
            }
            else
            {
                // jika input password diisi, hash password baru dengan bcrypt
                user.password = BCrypt.Net.BCrypt.HashPassword(user.password);
                passwordVerification = true;
            }

            bool result = _userService.EditUser(id, user);

            if (!result)
                return StatusCode(500, new { message = "Failed to update user" });

            return Ok(new { message = "User account updated successfully" });
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "admin")]
        public IActionResult DeleteUser(Guid id)
        {
            bool result = _userService.DeleteUser(id);

            if (!result)
                return StatusCode(500, new { message = "Failed to delete user" });

            return Ok(new { message = "User account deleted successfully" });
        }

        [HttpGet("ActivateUser")]
        public IActionResult ActivateUser(Guid id_user, string email)
        {
            try
            {
                User? user = _userService.CheckUser(email);

                if (user == null) { 
                    return BadRequest(new { message = "User not found" });
                }

                if (user.is_active)
                {
                    return BadRequest(new { message = "User has been activated" });
                }

                bool activatedUser = _userService.ActivateUser(id_user);

                if (activatedUser)
                {
                    return Ok(new { message = "Activated" });
                }
                else
                {
                    return StatusCode(500, new { message = "Activation Failed" });
                }


            }
            catch (Exception)
            {
                return StatusCode(500, new { message = "Activation Failed" });

            }
        }

        private async Task<bool> SendEmail(User user)
        {
            if (user == null)
            {
                return false;
            }

            if (string.IsNullOrEmpty(user.email))
            {
                return false;
            }

            List<string> to = new List<string>();
            to.Add(user.email);

            string subject = "Account Activation";
            string baseUrl = _configuration.GetSection("webAllowed").Value;

            string callBackUrl = baseUrl+"/activation/" + user.email + "/" + user.id.ToString();

            string body = @"
    <html>
      <head>
        <style>
          /* Styles for the email body */
          * {
            font-family: Arial, sans-serif;
            box-sizing: border-box;
          }
          body {
            margin: 0;
            padding: 0;
            background-color: #f5f5f5;
          }
          .email-container {
            max-width: 600px;
            margin: 0 auto;
            background-color: #ffffff;
            border-radius: 10px;
            overflow: hidden;
            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
          }
          .header {
            background-color: #f2f2f2;
            padding: 20px;
          }
          .header h1 {
            margin: 0;
            color: #333333;
            font-size: 24px;
          }
          .body {
            padding: 20px;
            line-height: 1.5;
          }
          .call-to-action {
            display: block;
            text-align: center;
            padding: 20px 0;
            background-color: #f2f2f2;
          }
          .call-to-action a {
            display: inline-block;
            padding: 12px 30px;
            background-color: #007bff;
            color: #ffffff;
            border-radius: 5px;
            text-decoration: none;
            font-weight: bold;
          }
          .footer {
            background-color: #f2f2f2;
            padding: 20px;
            text-align: center;
            color: #555555;
            font-size: 14px;
          }
        </style>
      </head>
      <body>
        <div class=""email-container"">
          <div class=""header"">
            <h1>Welcome to DLanguage!</h1>
          </div>
          <div class=""body"">
            <p>Dear " + user.nama + @",</p>
            <p>We are excited to have you as a new member of DLanguage! To activate your account, please click on the following link:</p>
             <div class=""call-to-action"">
            <a href=""" + callBackUrl + @""">Activate my account</a>
          </div>
            <p>Thank you for choosing us.</p>
            <p>Best regards,</p>
            <p>The Team</p>
          </div>
         
          <div class=""footer"">
            <p>This email was sent to " + user.email + @". If you did not create an account with us, please ignore this email.</p>
          </div>
        </div>
      </body>
    </html>";

            EmailModel model = new EmailModel(to, subject, body);

            bool result = await _emailService.SendAsync(model, new CancellationToken());

            return result;
        }


        [HttpPost("forgotpassword")]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordDto forgotPasswordDto)
        {
            if (string.IsNullOrEmpty(forgotPasswordDto.email))
                return BadRequest(new { message = "Email cannot be empty" });

            User user = _userService.CheckUser(forgotPasswordDto.email);

            if (user == null)
                return BadRequest(new { message = "User not found" });

            var secretKey = _configuration.GetSection("JwTConfig:Key").Value;
            var signingKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secretKey));

            // Create JWT token with user ID as the subject and set expiration time to 1 hour
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[] { new Claim(ClaimTypes.NameIdentifier, user.id.ToString()) }),
                Expires = DateTime.UtcNow.AddHours(1),
                SigningCredentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256Signature)
            };

            var tokenHandler = new JwtSecurityTokenHandler();

            string token = tokenHandler.WriteToken(tokenHandler.CreateToken(tokenDescriptor));

            // Send email with password reset link and token as query parameter
            List<string> to = new List<string>();
            to.Add(user.email);

            string subject = "Password Reset";

            string baseUrl = _configuration.GetSection("webAllowed").Value;

            string callBackUrl = baseUrl+"/newpassword/" + user.email +"/" + token;

            string body = @"
    <html>
      <head>
        <style>
          /* Styles for the email body */
          * {
            font-family: Arial, sans-serif;
            box-sizing: border-box;
          }
          body {
            margin: 0;
            padding: 0;
            background-color: #f5f5f5;
          }
          .email-container {
            max-width: 600px;
            margin: 0 auto;
            background-color: #ffffff;
            border-radius: 10px;
            overflow: hidden;
            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
          }
          .header {
            background-color: #f2f2f2;
            padding: 20px;
          }
          .header h1 {
            margin: 0;
            color: #333333;
            font-size: 24px;
          }
          .body {
            padding: 20px;
            line-height: 1.5;
          }
          .call-to-action {
            display: block;
            text-align: center;
            padding: 20px 0;
            background-color: #f2f2f2;
          }
          .call-to-action a {
            display: inline-block;
            padding: 12px 30px;
            background-color: #007bff;
            color: #ffffff;
            border-radius: 5px;
            text-decoration: none;
            font-weight: bold;
          }
          .footer {
            background-color: #f2f2f2;
            padding: 20px;
            text-align: center;
            color: #555555;
            font-size: 14px;
          }
        </style>
      </head>
      <body>
        <div class=""email-container"">
          <div class=""header"">
            <h1>Reset Password</h1>
          </div>
          <div class=""body"">
            <p>Dear " + user.nama + @",</p>
            <p>You have requested to reset your password. Please click on the following link:</p>
             <div class=""call-to-action"">
            <a href=""" + callBackUrl + @""">Reset Password</a>
          </div>
            <p>This link is valid for 1 hour only.</p>
            <p>Best regards,</p>
            <p>The Team</p>
          </div>
         
          <div class=""footer"">
            <p>This email was sent to " + user.email + @".If you did not request to reset your password, please ignore this email.</p>
          </div>
        </div>
      </body>
    </html>";
          

            EmailModel model = new EmailModel(to, subject, body);

            bool result = await _emailService.SendAsync(model, new CancellationToken());

            return Ok(new { message = "Password reset link sent to email", key = token });
        }


        [HttpPost("newpassword")]
        public IActionResult Register(string email, NewPasswordDto newPasswordDto, string token)
        {
            if (newPasswordDto.password != newPasswordDto.verifpassword)
                return BadRequest(new { message = "Password does not match" });

            User user = _userService.CheckUser(email);

            // Validate if user exists
            if (user == null)
                return BadRequest(new { message = "User not found" });

            var secretKey = _configuration.GetSection("JwTConfig:Key").Value;
            var jwtKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secretKey));

            try
            {
                // Validate token
                var tokenHandler = new JwtSecurityTokenHandler();
                var claimsPrincipal = tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    IssuerSigningKey = jwtKey,
                    ValidateLifetime = true,
                    ClockSkew = TimeSpan.Zero
                }, out SecurityToken validatedToken);

                // Get user ID from token
                var userId = claimsPrincipal.FindFirst(ClaimTypes.NameIdentifier).Value;

                // Check if the user ID in the token is the same as the email provided
                if (userId != user.id.ToString())
                    return BadRequest(new { message = "Invalid token" });

                // Check if token has expired
                if (validatedToken.ValidTo < DateTime.UtcNow)
                    return BadRequest(new { message = "Token has expired" });

                // Set new password using BCrypt
                var password = BCrypt.Net.BCrypt.HashPassword(newPasswordDto.password);
                bool result = _userService.NewPassword(email, password);

                if (!result)
                    return StatusCode(500, new { error = "Failed to update password" });

                return Ok(new { message = "Password updated successfully" });
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }



    }
}
