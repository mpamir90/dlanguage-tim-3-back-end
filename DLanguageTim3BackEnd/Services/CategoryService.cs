﻿using System.Data.SqlClient;
using DLanguageTim3BackEnd.Models;

namespace DLanguageTim3BackEnd.Services
{
    public class CategoryService
    {
        private readonly IConfiguration _configuration;
        private readonly string ConnectionString;

        public CategoryService(IConfiguration configuration)
        {
            _configuration = configuration;
            ConnectionString = configuration.GetConnectionString("DefaultConnection") ?? "No Configuration database";
        }

        public List<CategoryModel> GetAll()
        {
            List<CategoryModel> listClass = new List<CategoryModel>();

            string query = "SELECT * FROM categories";

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        connection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                listClass.Add(new CategoryModel
                                {
                                    Id = Convert.ToInt32(reader["id"]),
                                    Category = reader["category"].ToString() ?? string.Empty,
                                    ImgUrl = reader["img_url"].ToString() ?? string.Empty,
                                    BigImgUrl = reader["big_img_url"].ToString() ?? string.Empty,
                                    Description = reader["description"].ToString() ?? string.Empty
                                });
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return listClass;
        }

        public CategoryModel? GetById(int id)
        {
            CategoryModel? category = null;

            string query = $"SELECT * FROM categories WHERE id = @Id";

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    try
                    {
                        command.Connection = connection;
                        command.Parameters.Clear();
                        command.CommandText = query;

                        connection.Open();
                        command.Parameters.AddWithValue("@Id", id);

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                category = new CategoryModel
                                {
                                    Id = Convert.ToInt32(reader["id"]),
                                    Category = reader["category"].ToString() ?? string.Empty,
                                    ImgUrl = reader["img_url"].ToString() ?? string.Empty,
                                    BigImgUrl = reader["big_img_url"].ToString() ?? string.Empty,
                                    Description = reader["description"].ToString() ?? string.Empty
                                };
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return category;
        }

        public void Add(CategoryModel category)
        {
            string query = "INSERT INTO categories (category, img_url,big_img_url,description) VALUES (@Category, @ImgUrl,@BigImgUrl,@Description)";

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        connection.Open();

                        command.Parameters.AddWithValue("@Category", category.Category);
                        command.Parameters.AddWithValue("@ImgUrl", category.ImgUrl);
                        command.Parameters.AddWithValue("@BigImgUrl", category.BigImgUrl);
                        command.Parameters.AddWithValue("@Description", category.Description);

                        int rowsAffected = command.ExecuteNonQuery();

                        if (rowsAffected == 0)
                        {
                            throw new Exception("No rows were affected by the insert statement.");
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
        }

        public void Update(int id, CategoryModel category)
        {
            string query = "UPDATE categories SET category = @Category, img_url = @ImgUrl, big_img_url = @BigImgUrl, description = @Description WHERE id = @Id";

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        connection.Open();

                        command.Parameters.AddWithValue("@Category", category.Category);
                        command.Parameters.AddWithValue("@ImgUrl", category.ImgUrl);
                        command.Parameters.AddWithValue("@BigImgUrl", category.BigImgUrl);
                        command.Parameters.AddWithValue("@Description", category.Description);
                        command.Parameters.AddWithValue("@Id", id);

                        int rowsAffected = command.ExecuteNonQuery();

                        if (rowsAffected == 0)
                        {
                            throw new Exception("No rows were affected by the update statement.");
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
        }
        public void Delete(int id)
        {
            string query = "DELETE FROM categories WHERE id = @Id";

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        connection.Open();

                        command.Parameters.AddWithValue("@Id", id);

                        int rowsAffected = command.ExecuteNonQuery();

                        if (rowsAffected == 0)
                        {
                            throw new Exception("No rows were affected by the delete statement.");
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
        }

    }
}
