﻿using DLanguageTim3BackEnd.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace DLanguageTim3BackEnd.Services
{
    public class UserServices
    {
        private readonly IConfiguration _configuration;
        private readonly string ConnectionString;

        public UserServices(IConfiguration configuration)
        {
            _configuration = configuration;
            ConnectionString = _configuration.GetConnectionString("DefaultConnection");
        }

        public bool CreateUserAccount(User user)
        {
            bool result = false;

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                con.Open();

                SqlTransaction tran = con.BeginTransaction();

                try
                {
                    // First query
                    SqlCommand cmdUser = new SqlCommand();
                    cmdUser.Connection = con;
                    cmdUser.Transaction = tran;
                    cmdUser.Parameters.Clear();

                    cmdUser.CommandText = "INSERT INTO Users VALUES (@id, @nama, @email, @password, @is_active, @role)";
                    cmdUser.Parameters.AddWithValue("@id", user.id);
                    cmdUser.Parameters.AddWithValue("@nama", user.nama);
                    cmdUser.Parameters.AddWithValue("@email", user.email);
                    cmdUser.Parameters.AddWithValue("@password", user.password);
                    cmdUser.Parameters.AddWithValue("@is_active", user.is_active);
                    cmdUser.Parameters.AddWithValue("@role", user.role);

                    var resultUser = cmdUser.ExecuteNonQuery();

                    tran.Commit();

                    result = true;

                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw;
                }
                finally
                {
                    con.Close();
                }
            }

            return result;
        }

        public User? CheckUser(string email)
        {
            User? user = null;

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = con;
                    cmd.CommandText = "SELECT * FROM Users WHERE email = @email";

                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@email", email);

                    con.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            user = new User
                            {
                                id = Guid.Parse(reader["id"].ToString() ?? string.Empty),
                                nama = reader["nama"].ToString() ?? string.Empty,
                                email = reader["email"].ToString() ?? string.Empty,
                                password = reader["password"].ToString() ?? string.Empty,
                                is_active = Convert.ToBoolean(reader["is_active"]),
                                role = reader["role"].ToString() ?? string.Empty
                            };
                        }
                    }

                    con.Close();
                }
            }

            return user;
        }

        public List<User> GetAllUsers()
        {
            List<User> userList = new List<User>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = con;
                    cmd.CommandText = "SELECT * FROM Users";

                    con.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            User user = new User
                            {
                                id = Guid.Parse(reader["id"].ToString() ?? string.Empty),
                                nama = reader["nama"].ToString() ?? string.Empty,
                                email = reader["email"].ToString() ?? string.Empty,
                                is_active = Convert.ToBoolean(reader["is_active"]),
                                role = reader["role"].ToString() ?? string.Empty
                            };

                            userList.Add(user);
                        }
                    }

                    con.Close();
                }
            }

            return userList;
        }
        public User? GetUserById(Guid id)
        {
            User? user = null;

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = con;
                    cmd.CommandText = "SELECT * FROM Users WHERE id = @id";

                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@id", id);

                    con.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            user = new User
                            {
                                id = Guid.Parse(reader["id"].ToString() ?? string.Empty),
                                nama = reader["nama"].ToString() ?? string.Empty,
                                email = reader["email"].ToString() ?? string.Empty,
                                password = reader["password"].ToString() ?? string.Empty,
                                is_active = Convert.ToBoolean(reader["is_active"]),
                                role = reader["role"].ToString() ?? string.Empty
                            };
                        }
                    }

                    con.Close();
                }
            }

            return user;
        }

        public bool ActivateUser(Guid id)
        {
            bool result = true;

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.Parameters.Clear();

                cmd.CommandText = "UPDATE users SET is_active = 1 WHERE id = @id";
                cmd.Parameters.AddWithValue("@id", id);

                con.Open();
                result = cmd.ExecuteNonQuery() > 0 ? true : false;
                con.Close();
            }

            return result;
        }

        public bool EditUser(Guid id, User user)
        {
            bool result = true;

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.Parameters.Clear();

                cmd.CommandText = "UPDATE Users SET nama = @nama, email = @email, password = @password,is_active = @is_active, role = @role WHERE id = @id";

                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@nama", user.nama);
                cmd.Parameters.AddWithValue("@email", user.email);
                cmd.Parameters.AddWithValue("@password", user.password);
                cmd.Parameters.AddWithValue("@is_active", user.is_active);
                cmd.Parameters.AddWithValue("@role", user.role);

                con.Open();
                result = cmd.ExecuteNonQuery() > 0 ? true : false;
                con.Close();
            }

            return result;
        }

        public bool DeleteUser(Guid id)
        {
            bool result = true;

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.Parameters.Clear();

                cmd.CommandText = "DELETE FROM Users WHERE id = @id";
                cmd.Parameters.AddWithValue("@id", id);

                con.Open();
                result = cmd.ExecuteNonQuery() > 0 ? true : false;
                con.Close();
            }

            return result;
        }

        public bool NewPassword(string email, string password )
        {
            bool result = true;

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.Parameters.Clear();

                cmd.CommandText = "UPDATE Users SET password = @password WHERE email = @email";
                cmd.Parameters.AddWithValue("@email", email);
                cmd.Parameters.AddWithValue("@password", password);

                con.Open();
                result = cmd.ExecuteNonQuery() > 0 ? true : false;
                con.Close();
            }

            return result;
        }

        
    }
}