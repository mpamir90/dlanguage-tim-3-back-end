﻿using DLanguageTim3BackEnd.Models;
using System.Data.SqlClient;

namespace DLanguageTim3BackEnd.Services
{
    public class PaymentServices
    {
        private readonly IConfiguration _configuration;
        private readonly string ConnectionString;

        public PaymentServices(IConfiguration configuration)
        {
            _configuration = configuration;
            ConnectionString = configuration.GetConnectionString("DefaultConnection") ?? "No Configuration database";
        }

        public List<Payment> GetAll()
        {
            List<Payment> listPayment = new List<Payment>();

            string query = "SELECT * FROM payments";

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        connection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                listPayment.Add(new Payment
                                {
                                    id = Convert.ToInt32(reader["id"]),
                                    payment = reader["payment"].ToString() ?? string.Empty,
                                    is_active = Convert.ToBoolean(reader["is_active"]),
                                    img_url = reader["img_url"].ToString() ?? string.Empty
                                });
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return listPayment;
        }

        public Payment? GetById(int id)
        {
            Payment? payment = null;

            string query = $"SELECT * FROM payments WHERE id = @id";

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    try
                    {
                        command.Connection = connection;
                        command.Parameters.Clear();
                        command.CommandText = query;

                        connection.Open();
                        command.Parameters.AddWithValue("@id", id);

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                payment = new Payment
                                {
                                    id = Convert.ToInt32(reader["id"]),
                                    payment = reader["payment"].ToString() ?? string.Empty,
                                    is_active = Convert.ToBoolean(reader["is_active"]),
                                    img_url = reader["img_url"].ToString() ?? string.Empty
                                };
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return payment;
        }

        public bool AddPayment(Payment payment)
        {
            string query = @"INSERT INTO payments (payment, is_active, img_url) VALUES (@payment, @isActive, @imgUrl)";

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        command.Parameters.AddWithValue("@payment", payment.payment);
                        command.Parameters.AddWithValue("@isActive", payment.is_active);
                        command.Parameters.AddWithValue("@imgUrl", payment.img_url);

                        connection.Open();

                        int rowsAffected = command.ExecuteNonQuery();

                        if (rowsAffected > 0)
                        {
                            return true;
                        }

                        return false;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
        }
        public bool UpdatePayment(int id, Payment payment)
        {
            string query = @"UPDATE payments SET payment = @payment, is_active = @isActive, img_url = @imgUrl WHERE id = @id";

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        command.Parameters.AddWithValue("@id", id);
                        command.Parameters.AddWithValue("@payment", payment.payment);
                        command.Parameters.AddWithValue("@isActive", payment.is_active);
                        command.Parameters.AddWithValue("@imgUrl", payment.img_url);

                        connection.Open();

                        int rowsAffected = command.ExecuteNonQuery();

                        if (rowsAffected > 0)
                        {
                            return true;
                        }

                        return false;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
        }
        public void Delete(int id)
        {
            string query = "DELETE FROM payments WHERE id = @Id";

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        connection.Open();

                        command.Parameters.AddWithValue("@Id", id);

                        int rowsAffected = command.ExecuteNonQuery();

                        if (rowsAffected == 0)
                        {
                            throw new Exception("No rows were affected by the delete statement.");
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
        }
    }
}
