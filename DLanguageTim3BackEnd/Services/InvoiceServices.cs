﻿using DLanguageTim3BackEnd.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace DLanguageTim3BackEnd.Services
{
    public class InvoiceServices
    {
        private readonly IConfiguration _configuration;
        private readonly string _connectionString;

        public InvoiceServices(IConfiguration configuration)
        {
            _configuration = configuration;
            _connectionString = configuration.GetConnectionString("DefaultConnection");
        }

        public List<Invoice> GetAllInvoices()
        {
            List<Invoice> invoiceList = new List<Invoice>();

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = con;
                    cmd.CommandText = "SELECT id, id_user, no_invoice, total_price, qty, created_at FROM my_invoices";

                    con.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Invoice invoice = new Invoice
                            {
                                id = Convert.ToInt32(reader["id"]),
                                id_user = reader["id_user"].ToString(),
                                no_invoice = reader["no_invoice"].ToString(),
                                total_price = Convert.ToInt32(reader["total_price"]),
                                qty = Convert.ToInt32(reader["qty"]),
                                created_at = Convert.ToDateTime(reader["created_at"])
                            };

                            invoiceList.Add(invoice);
                        }
                    }

                    con.Close();
                }
            }

            return invoiceList;
        }

        public List<Invoice> GetInvoiceByIdUser(string idUser)
        {
            List<Invoice> invoiceList = new List<Invoice>();

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = con;
                    cmd.CommandText = "SELECT id, id_user, no_invoice, total_price, qty, created_at FROM my_invoices WHERE id_user = @id_user";
                    cmd.Parameters.AddWithValue("@id_user", idUser);

                    con.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Invoice invoice = new Invoice
                            {
                                id = Convert.ToInt32(reader["id"]),
                                id_user = reader["id_user"].ToString(),
                                no_invoice = reader["no_invoice"].ToString(),
                                total_price = Convert.ToInt32(reader["total_price"]),
                                qty = Convert.ToInt32(reader["qty"]),
                                created_at = Convert.ToDateTime(reader["created_at"])
                            };

                            invoiceList.Add(invoice);
                        }
                    }

                    con.Close();
                }
            }

            return invoiceList;
        }

        public int CreateInvoice(Invoice invoice)
        {
            int invoiceId = 0;

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = con;
                    cmd.CommandText = "INSERT INTO my_invoices(id_user, no_invoice, total_price, qty, created_at, updated_at) VALUES (@id_user, @no_invoice, @total_price, @qty, @created_at, @updated_at); SELECT SCOPE_IDENTITY();";
                    cmd.Parameters.AddWithValue("@id_user", invoice.id_user);
                    cmd.Parameters.AddWithValue("@no_invoice", invoice.no_invoice);
                    cmd.Parameters.AddWithValue("@total_price", invoice.total_price);
                    cmd.Parameters.AddWithValue("@qty", invoice.qty);
                    cmd.Parameters.AddWithValue("@created_at", DateTime.Now);
                    cmd.Parameters.AddWithValue("@updated_at", DateTime.Now);

                    con.Open();

                    invoiceId = Convert.ToInt32(cmd.ExecuteScalar());

                    con.Close();
                }
            }

            return invoiceId;
        }

        public InvoiceDetailModel GetDetailInvoice(string noInvoice)
        {
            InvoiceDetailModel invoiceDetail = new InvoiceDetailModel();

            

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                

                try
                {
                    con.Open();

                    using (SqlCommand cmdInvoices = new SqlCommand())
                    {
                        cmdInvoices.Connection = con;
                        cmdInvoices.CommandText = "SELECT no_invoice,total_price,created_at FROM my_invoices WHERE no_invoice = @no_invoice";
                        cmdInvoices.Parameters.AddWithValue("@no_invoice", noInvoice);


                        using (SqlDataReader reader = cmdInvoices.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                invoiceDetail.created_at = DateTime.Parse(reader["created_at"].ToString() ?? string.Empty);
                                invoiceDetail.no_invoice = reader["no_invoice"].ToString() ?? string.Empty;
                                invoiceDetail.total_price = Convert.ToInt32(reader["total_price"].ToString());

                                
                            }
                        }


                    }

                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "SELECT class.name,my_class.schedule,categories.category,class.price FROM my_class INNER JOIN class ON my_class.id_class = class.id INNER JOIN categories ON class.id_category = categories.id WHERE my_class.no_invoice = @no_invoice";
                        cmd.Parameters.AddWithValue("@no_invoice", noInvoice);

                        

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            List<InvoiceDetailClassItem> invoiceList = new List<InvoiceDetailClassItem>();
                            while (reader.Read())
                            {
                                InvoiceDetailClassItem invoice = new InvoiceDetailClassItem
                                {
                                    category = reader["category"].ToString() ?? string.Empty,
                                    name = reader["name"].ToString() ?? string.Empty,
                                    price = Convert.ToInt32(reader["price"].ToString()),
                                    schedule = DateTime.Parse(reader["schedule"].ToString() ?? string.Empty),
                                };

                                invoiceList.Add(invoice);
                            }
                            invoiceDetail.list_class = invoiceList;
                        }


                    }


                }
                catch (Exception)
                {

                    throw;
                }
                finally
                {
                    con.Close();
                }
               
            }

            return invoiceDetail;
        }
    }
}
