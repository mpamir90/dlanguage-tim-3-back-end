﻿using DLanguageTim3BackEnd.Models;
using System.Data.SqlClient;

namespace DLanguageTim3BackEnd.Services
{
    public class MyClassService
    {
        private readonly IConfiguration _configuration;
        private readonly string ConnectionString;

        public MyClassService(IConfiguration configuration)
        {
            _configuration = configuration;
            ConnectionString = configuration.GetConnectionString("DefaultConnection") ?? "No Configuration database";
        }

        public List<MyClassModel> GetAllMyClass(Guid idUser)
        {
            List<MyClassModel> listMyClass = new List<MyClassModel>();

            string query = "SELECT my_class.id, my_class.id_user, my_class.id_class, my_class.schedule, my_class.created_at, my_class.updated_at, class.name, class.img_url, class.price, categories.category FROM my_class INNER JOIN class ON my_class.id_class = class.id INNER JOIN categories ON class.id_category = categories.id WHERE id_user = @id_user";

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        connection.Open();
                        command.Parameters.Clear();
                        command.Parameters.AddWithValue("@id_user", idUser);

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                listMyClass.Add(new MyClassModel
                                {
                                    Id = Convert.ToInt32(reader["id"]),
                                    Category = reader["category"].ToString() ?? string.Empty,
                                    ImgUrl = reader["img_url"].ToString() ?? string.Empty,
                                    IdClass = Convert.ToInt32(reader["id"]),
                                    Name = reader["name"].ToString() ?? string.Empty,
                                    Price = Convert.ToInt32(reader["price"]),
                                    Schedule = DateTime.Parse(reader["schedule"].ToString() ?? string.Empty),
                                    CreatedAt = DateTime.Parse(reader["created_at"].ToString() ?? string.Empty),
                                    UpdatedAt = DateTime.Parse(reader["updated_at"].ToString() ?? string.Empty)
                                });
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return listMyClass;
        }
    }
}
