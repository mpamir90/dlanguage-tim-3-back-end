﻿using DLanguageTim3BackEnd.DTOs;
using DLanguageTim3BackEnd.Models;
using System.Data.SqlClient;

namespace DLanguageTim3BackEnd.Services
{
    public class CheckoutService
    {
        private readonly IConfiguration _configuration;
        private readonly string ConnectionString;

        public CheckoutService(IConfiguration configuration)
        {
            _configuration = configuration;
            ConnectionString = configuration.GetConnectionString("DefaultConnection") ?? "No Configuration database";
        }

        public bool AddCheckoutItem(CheckoutModel checkout)
        {
            bool result = false;

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                con.Open();

                SqlTransaction tran = con.BeginTransaction();

                try
                {
                    // First query
                    SqlCommand cmdUser = new SqlCommand();
                    cmdUser.Connection = con;
                    cmdUser.Transaction = tran;
                    cmdUser.Parameters.Clear();

                    cmdUser.CommandText = "INSERT INTO my_checkout(id_user,id_class,schedule) VALUES(@id_user, @id_class, @schedule)";
                    cmdUser.Parameters.AddWithValue("@id_user", checkout.IdUser);
                    cmdUser.Parameters.AddWithValue("@id_class", checkout.IdClass);
                    cmdUser.Parameters.AddWithValue("@schedule", checkout.schedule);



                    var resultUser = cmdUser.ExecuteNonQuery();

                    tran.Commit();

                    result = true;

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    tran.Rollback();
                    throw;
                }
                finally
                {
                    con.Close();
                }
            }

            return result;
        }

        public bool CheckItemCheckoutUser(Guid idUser, int idClass,DateTime schedule)
        {
            bool res = false;

            string query = "SELECT id FROM my_checkout WHERE id_class = @id_class AND id_user = @id_user AND schedule = @schedule";

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            using (SqlCommand command = new SqlCommand(query, connection))
            {
                command.Parameters.AddWithValue("@id_user", idUser);
                command.Parameters.AddWithValue("@schedule", schedule);
                command.Parameters.AddWithValue("@id_class", idClass);

                try
                {
                    connection.Open();
                    object? result = command.ExecuteScalar();
                    if (result != null)
                    {
                        res = true;
                    }

                    
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    throw;
                }

                return res;
            }
        }

        public bool CheckItemFromMyClass(Guid idUser, DateTime schedule,int idClass)
        {
            bool res = false;

            string query = "SELECT id FROM my_class WHERE id_class = @id_class AND id_user = @id_user AND schedule = @schedule";

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            using (SqlCommand command = new SqlCommand(query, connection))
            {
                command.Parameters.AddWithValue("@id_user", idUser);
                command.Parameters.AddWithValue("@schedule", schedule);
                command.Parameters.AddWithValue("@id_class", idClass);

                try
                {
                    connection.Open();
                    object? result = command.ExecuteScalar();
                    if (result != null)
                    {
                        res = true;
                    }


                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    throw;
                }

                return res;
            }
        }

        public List<CheckoutUserModel> GetAllById(Guid idUser)
        {
            List<CheckoutUserModel> listCheckoutUserModel = new List<CheckoutUserModel>();

            string query = "SELECT my_checkout.id, my_checkout.id_class, my_checkout.schedule, class.name, class.price, class.img_url, categories.category FROM my_checkout INNER JOIN class ON my_checkout.id_class = class.id INNER JOIN categories ON class.id_category = categories.id WHERE id_user = @id_user";

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        connection.Open();
                        command.Parameters.Clear();
                        command.Parameters.AddWithValue("@id_user", idUser);

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                listCheckoutUserModel.Add(new CheckoutUserModel
                                {
                                    id = Convert.ToInt32(reader["id"]),
                                    name = reader["name"].ToString() ?? string.Empty,
                                    price = Convert.ToInt32(reader["price"]),
                                    schedule = DateTime.Parse(reader["schedule"].ToString() ?? string.Empty),
                                    category = reader["category"].ToString() ?? string.Empty,
                                    img_url = reader["img_url"].ToString() ?? string.Empty,
                                    id_class = Convert.ToInt32(reader["id_class"]),
                                });
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return listCheckoutUserModel;
        }

        public void Delete(int id)
        {
            string query = "DELETE FROM my_checkout WHERE id = @id";

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        connection.Open();

                        command.Parameters.AddWithValue("@id", id);

                        int rowsAffected = command.ExecuteNonQuery();

                        if (rowsAffected == 0)
                        {
                            throw new Exception("No rows were affected by the delete statement.");
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
        }

        public bool PayNow(CheckoutPayDto checkoutPayDto)
        {
            bool result = false;

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                con.Open();

                SqlTransaction tran = con.BeginTransaction();

                try
                {
                    var now = DateTime.Now;
                    int resultUser = 0;
                    var noInvoice = GenerateInvoiceNumber();

                    if (noInvoice == null)
                    {
                        Console.WriteLine($"No invoice is: ${noInvoice}");
                        return false;
                    }

                    //query my_invoices
                    SqlCommand cmdInvoice = new SqlCommand();
                    cmdInvoice.Connection = con;
                    cmdInvoice.Transaction = tran;
                    cmdInvoice.Parameters.Clear();

                    cmdInvoice.CommandText = "INSERT INTO my_invoices ( id_user, no_invoice, qty, total_price, updated_at, created_at ) VALUES ( @id_user, @no_invoice, @qty, @total_price, @updated_at, @created_at )";
                    cmdInvoice.Parameters.AddWithValue("@id_user", checkoutPayDto.id_user);
                    cmdInvoice.Parameters.AddWithValue("@no_invoice", noInvoice);
                    cmdInvoice.Parameters.AddWithValue("@qty", checkoutPayDto.list_checkout.Count);
                    int totalPrice = checkoutPayDto.list_checkout.Sum(c => c.price);
                    cmdInvoice.Parameters.AddWithValue("@total_price", totalPrice);
                    cmdInvoice.Parameters.AddWithValue("@updated_at", now);
                    cmdInvoice.Parameters.AddWithValue("@created_at", now);

                    var resultInvoice = cmdInvoice.ExecuteNonQuery();
                    Console.WriteLine($"Invoice Rows: ${resultInvoice}");

                    // query(my_class)
                    for (int i = 0; i < checkoutPayDto.list_checkout.Count; i++)
                    {
                        SqlCommand cmdUser = new SqlCommand();
                        cmdUser.Connection = con;
                        cmdUser.Transaction = tran;
                        cmdUser.Parameters.Clear();

                        cmdUser.CommandText = "INSERT INTO my_class ( id_user, id_class, schedule, created_at, updated_at, no_invoice ) VALUES ( @id_user, @id_class, @schedule, @created_at, @updated_at,@no_invoice )";
                        cmdUser.Parameters.AddWithValue("@id_user", checkoutPayDto.id_user);
                        cmdUser.Parameters.AddWithValue("@id_class", checkoutPayDto.list_checkout[i].id_class);
                        cmdUser.Parameters.AddWithValue("@schedule", checkoutPayDto.list_checkout[i].schedule);
                        cmdUser.Parameters.AddWithValue("@no_invoice", noInvoice);
                        cmdUser.Parameters.AddWithValue("@created_at", now);
                        cmdUser.Parameters.AddWithValue("@updated_at", now);

                        resultUser = cmdUser.ExecuteNonQuery();
                        Console.WriteLine($"my_class rows: ${resultUser.ToString()}");
                    }

                    for (int i = 0; i < checkoutPayDto.list_checkout.Count; i++)
                    {
                        SqlCommand cmdDel = new SqlCommand();
                        cmdDel.Connection = con;
                        cmdDel.Transaction = tran;
                        cmdDel.Parameters.Clear();

                        cmdDel.CommandText = "DELETE FROM my_checkout WHERE id = @id";
                        cmdDel.Parameters.AddWithValue("@id", checkoutPayDto.list_checkout[i].id);

                        int resultDel = cmdDel.ExecuteNonQuery();
                        Console.WriteLine($"Delete rows: ${resultDel.ToString()}");
                    }

                    

                    tran.Commit();

                    result = true;

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    tran.Rollback();
                    throw;
                }
                finally
                {
                    con.Close();
                }
            }

            return result;
        }

        private string GenerateInvoiceNumber()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                var command = new SqlCommand("SELECT TOP 1 no_invoice FROM my_invoices ORDER BY created_at DESC", connection);
                var lastInvoiceNumber = (string)command.ExecuteScalar();

                if (lastInvoiceNumber != null)
                {
                    var lastNumber = lastInvoiceNumber.Substring(3);
                    var newNumber = int.Parse(lastNumber) + 1;
                    return $"DLA{newNumber:000}";
                }
                else
                {
                    return "DLA001";
                }
            }
        }

    }
}
