﻿using System.Data.SqlClient;
using DLanguageTim3BackEnd.Models;

namespace DLanguageTim3BackEnd.Services
{
    public class ClassServices
    {
        private readonly IConfiguration _configuration;
        private readonly string ConnectionString;

        public ClassServices(IConfiguration configuration)
        {
            _configuration = configuration;
            ConnectionString = configuration.GetConnectionString("DefaultConnection") ?? "No Configuration database";
        }

        public List<ClassModel> GetAll()
        {
            List<ClassModel> listClass = new List<ClassModel>();

            string query = "SELECT class.id, class.description, categories.category, class.img_url, class.name, class.price,class.id_category FROM class INNER JOIN categories ON class.id_category = categories.id";

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        connection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Console.WriteLine(reader);
                                listClass.Add(new ClassModel
                                {
                                    Id = Convert.ToInt32(reader["id"]),
                                    Name = reader["name"].ToString() ?? string.Empty,
                                    Description = reader["description"].ToString() ?? string.Empty,
                                    IdCategory = Convert.ToInt32(reader["id_category"]),
                                    ImgUrl = reader["img_url"].ToString() ?? string.Empty,
                                    price = Convert.ToInt32(reader["price"]),
                                    category = reader["category"].ToString() ?? string.Empty
                                });
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return listClass;
        }

        public ClassModel? GetById(int id)
        {
            ClassModel? theClass = null;
            
            string query = $"SELECT class.id, class.description, categories.category, class.img_url, class.name, class.price,class.id_category FROM class INNER JOIN categories ON class.id_category = categories.id WHERE class.id = @Id; ";

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    try
                    {
                        command.Connection = connection;
                        command.Parameters.Clear();
                        command.CommandText = query;

                        connection.Open();
                        command.Parameters.AddWithValue("@Id", id);

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                theClass = new ClassModel
                                {
                                    Id = Convert.ToInt32(reader["id"]),
                                    Name = reader["name"].ToString() ?? string.Empty,
                                    Description = reader["description"].ToString() ?? string.Empty,
                                    IdCategory = Convert.ToInt32(reader["id_category"]),
                                    ImgUrl = reader["img_url"].ToString() ?? string.Empty,
                                    price = Convert.ToInt32(reader["price"]),
                                    category = reader["category"].ToString() ?? string.Empty
                                };
                            }
                        }

                        if (theClass != null)
                        {
                            List<DateTime> listSchedule = new List<DateTime>();

                            // Tambahkan tanggal-tanggal 7 hari ke depan, dimulai dari hari ini
                            for (int i = 0; i < 7; i++)
                            {
                                DateTime date = DateTime.Today.AddDays(i);
                                if (date.DayOfWeek != DayOfWeek.Sunday)
                                {
                                    listSchedule.Add(date);
                                }
                            }

                            theClass.ListSchedule = listSchedule;
                        }
                        

                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return theClass;
        }

        public List<ClassModel> GetAllByCategory(int idCategory)
        {
            List<ClassModel> listClass = new List<ClassModel>();

            string query = "SELECT class.id, class.description, categories.category, class.img_url, class.name, class.price,class.id_category FROM class INNER JOIN categories ON class.id_category = categories.id WHERE class.id_category = @id_category";

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        connection.Open();
                        command.Parameters.Clear();
                        command.Parameters.AddWithValue("@id_category", idCategory);

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Console.WriteLine(reader);
                                listClass.Add(new ClassModel
                                {
                                    Id = Convert.ToInt32(reader["id"]),
                                    Name = reader["name"].ToString() ?? string.Empty,
                                    Description = reader["description"].ToString() ?? string.Empty,
                                    IdCategory = Convert.ToInt32(reader["id_category"]),
                                    ImgUrl = reader["img_url"].ToString() ?? string.Empty,
                                    price = Convert.ToInt32(reader["price"]),
                                    category = reader["category"].ToString() ?? string.Empty
                                });
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return listClass;
        }

        public void AddClass(ClassModel classModel)
        {
            string query = "INSERT INTO class (name, description, id_category, img_url, price) VALUES (@Name, @Description, @IdCategory, @ImgUrl, @Price)";

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        connection.Open();

                        command.Parameters.AddWithValue("@Name", classModel.Name);
                        command.Parameters.AddWithValue("@Description", classModel.Description);
                        command.Parameters.AddWithValue("@IdCategory", classModel.IdCategory);
                        command.Parameters.AddWithValue("@ImgUrl", classModel.ImgUrl);
                        command.Parameters.AddWithValue("@Price", classModel.price);

                        command.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }

            }
        }
        public bool UpdateClass(int id, ClassModel classModel)
        {
            string query = @"UPDATE class SET name = @name, description = @description, id_category = @id_category,price = @price,img_url = @img_url  WHERE id = @id";

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        command.Parameters.AddWithValue("@id", id);
                        command.Parameters.AddWithValue("@name", classModel.Name);
                         command.Parameters.AddWithValue("@description", classModel.Description);
                         command.Parameters.AddWithValue("@id_category", classModel.IdCategory);
                         command.Parameters.AddWithValue("@price", classModel.price);
                         command.Parameters.AddWithValue("@img_url", classModel.ImgUrl);

                        connection.Open();

                        int rowsAffected = command.ExecuteNonQuery();

                        if (rowsAffected > 0)
                        {
                            return true;
                        }

                        return false;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
        }
        public void Delete(int id)
        {
            string query = "DELETE FROM class WHERE id = @Id";

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        connection.Open();

                        command.Parameters.AddWithValue("@Id", id);

                        int rowsAffected = command.ExecuteNonQuery();

                        if (rowsAffected == 0)
                        {
                            throw new Exception("No rows were affected by the delete statement.");
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
        }

        public List<ClassScheduleModel> GetClassSchedule(string id)
        {
            List<ClassScheduleModel> listClassSchedule = new List<ClassScheduleModel>();

            string query = "SELECT schedule_class.id,schedule_class.schedule FROM schedule_class INNER JOIN class ON schedule_class.id_class = class.id WHERE id_class = @Id";

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        command.CommandText = query;

                        connection.Open();
                        command.Parameters.AddWithValue("@Id", id);

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Console.WriteLine(reader);
                                listClassSchedule.Add(new ClassScheduleModel
                                {
                                    Id = Convert.ToInt32(reader["id"]),
                                    Schedule = DateTime.Parse(reader["schedule"].ToString() ?? string.Empty),
                                }); ;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return listClassSchedule;
        }
    }
}
